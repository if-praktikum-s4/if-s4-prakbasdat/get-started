# Prepare Postgre SQL

Installer PostgreSQL for Windows: [Klik di sini](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)
## 1. Instalasi
![](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/get-started/-/raw/main/img/postgresql-installing.png)
## 2. Konfigurasi
Isi password, hostname, dan port yang akan kita gunakan
![](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/get-started/-/raw/main/img/postgre-configure.png)
## 3. Connect with Dbeaver
![](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/get-started/-/raw/main/img/connect-w-dbeaver.png)
